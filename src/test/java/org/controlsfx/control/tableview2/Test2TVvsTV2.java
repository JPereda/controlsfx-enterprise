package org.controlsfx.control.tableview2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.control.tableview2.cell.ComboBox2TableCell;
import org.controlsfx.control.tableview2.cell.TextField2TableCell;

public class Test2TVvsTV2 extends Application {
    private final ObservableList<Person> data = Person.generateData(100);
    private final ObservableList<Person> dataOrig = Person.generateData(100);
    
    private final TableView2<Person> table = new TableView2<>();
    private final TableColumn2<Person, String> firstName = new TableColumn2<>("First Name");
    private final TableColumn2<Person, String> lastName = new TableColumn2<>("Last Name");
    private final TableColumn2<Person, String> city = new TableColumn2<>("City");
    private final TableColumn2<Person, LocalDate> birthday = new TableColumn2<>("Birthday");
    private final TableColumn2<Person, Boolean> active = new TableColumn2<>("Active");
    
    private final TableView<Person> tableOrig = new TableView<>();
    private final TableColumn<Person, String> firstNameOrig = new TableColumn<>("First Name");
    private final TableColumn<Person, String> lastNameOrig = new TableColumn<>("Last Name");
    private final TableColumn<Person, String> cityOrig = new TableColumn<>("City");
    private final TableColumn<Person, LocalDate> birthdayOrig = new TableColumn<>("Birthday");
    private final TableColumn<Person, Boolean> activeOrig = new TableColumn<>("Active");
    
    private final VBox controls = new VBox(10);
    
    @Override
    public void start(Stage primaryStage) {
        setTableView();
        setTableView2();
        setControls();
        final HBox hBox = new HBox(50, tableOrig, controls, table);
        HBox.setHgrow(tableOrig, Priority.ALWAYS);
        HBox.setHgrow(table, Priority.ALWAYS);
        hBox.setPadding(new Insets(20));
        Scene scene = new Scene(hBox, 1200, 500);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
       
        table.getFixedRows().addAll(0, 1, 2);
//        table.getFixedColumns().add(fullNameColumn);
//        table.getFixedColumns().addAll(firstName, lastName);
    }
    
    private void setTableView2() {
        firstName.setCellValueFactory(p -> p.getValue().firstNameProperty());
//        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setCellFactory(ComboBox2TableCell.forTableColumn("Name 1", "Name 2", "Name 3", "Name 4"));
        firstName.setPrefWidth(110);
        
        lastName.setCellValueFactory(p -> p.getValue().lastNameProperty());
//        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setCellFactory(TextField2TableCell.forTableColumn());
        lastName.setPrefWidth(130);
        
        city.setCellValueFactory(p -> p.getValue().cityProperty());
//        city.setCellFactory(TextFieldTableCell.forTableColumn());
        city.setCellFactory(p -> new TableCell<Person, String>() {
            
            private Button button;
            {
                button = new Button();
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty); 
                if (item != null && ! empty) {
                    button.setText(item);
//                    if (getIndex() % 5 == 0) {
//                        setStyle("-fx-font-size: 2em;");
//                    } else {
//                        setStyle("-fx-font-size: 1em;");
//                    }
                    setText(null);
                    setGraphic(button);
                } else {
                    setText(null);
                    setGraphic(null);
                    setStyle(null);
                }
            }
            
        });
//        city.setStyle("-fx-font-size: 2em;");
        
        birthday.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthday.setPrefWidth(100);
        birthday.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        active.setText("Active");
        active.setCellValueFactory(p -> p.getValue().activeProperty());
        active.setCellFactory(CheckBoxTableCell.forTableColumn(active));
        active.setPrefWidth(60);
        
        table.setItems(data);
        TableColumn2 fullNameColumn = new TableColumn2("Full Name");
        fullNameColumn.getColumns().addAll(firstName, lastName);
        table.getColumns().addAll(fullNameColumn, city, birthday, active);
//        table.getColumns().addAll(firstName, lastName, city, birthday, active);
    }
    
    private void setTableView() {
        firstNameOrig.setCellValueFactory(p -> p.getValue().firstNameProperty());
        //firstNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameOrig.setCellFactory(p -> {
            ComboBoxTableCell<Person, String> cell = new ComboBoxTableCell<>("Name 1", "Name 2", "Name 3", "Name 4");
            cell.setComboBoxEditable(true);
            return cell;
        });
        firstNameOrig.setPrefWidth(110);
        
        lastNameOrig.setCellValueFactory(p -> p.getValue().lastNameProperty());
        lastNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameOrig.setPrefWidth(130);
        
        cityOrig.setCellValueFactory(p -> p.getValue().cityProperty());
        cityOrig.setCellFactory(TextFieldTableCell.forTableColumn());
//        cityOrig.setStyle("-fx-font-size: 2em;");
        
        birthdayOrig.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthdayOrig.setPrefWidth(100);
        birthdayOrig.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        activeOrig.setText("Active");
        activeOrig.setCellValueFactory(p -> p.getValue().activeProperty());
        activeOrig.setCellFactory(CheckBoxTableCell.forTableColumn(activeOrig));
        activeOrig.setPrefWidth(60);
        
        tableOrig.setItems(dataOrig);
        TableColumn column = new TableColumn("Full Name");
        column.getColumns().addAll(firstNameOrig, lastNameOrig);
        tableOrig.getColumns().addAll(column, cityOrig, birthdayOrig, activeOrig);
        
    }
    
    private void setControls() {
    
        CheckBox tableEditionEnabled = new CheckBox("Table Edition Enabled");
        tableEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.setEditable(nv);
            tableOrig.setEditable(nv);
        });
        tableEditionEnabled.setSelected(true);
        
        CheckBox columnsEditionEnabled = new CheckBox("Columns Edition Enabled");
        columnsEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
            tableOrig.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
        });
        columnsEditionEnabled.setSelected(true);
        
        CheckBox cellSelectionEnabled = new CheckBox("Cell Selection Enabled");
        cellSelectionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setCellSelectionEnabled(nv);
            tableOrig.getSelectionModel().setCellSelectionEnabled(nv);
        });
        
        CheckBox selectionMultiple = new CheckBox("Selection Multiple");
        selectionMultiple.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
            tableOrig.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
        });
        
        CheckBox columnPolicy = new CheckBox("Column Policy Constrained");
        columnPolicy.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
            tableOrig.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
        });
        
        CheckBox fixedCellSize = new CheckBox("Set Fixed Cell Size");
        fixedCellSize.selectedProperty().addListener((obs, ov, nv) -> {
            table.setFixedCellSize(nv ? 40 : 0);
            tableOrig.setFixedCellSize(nv ? 40 : 0);
        });
        
        CheckBox showTableMenuButton = new CheckBox("Show Table Menu Button");
        showTableMenuButton.selectedProperty().addListener((obs, ov, nv) -> {
            table.setTableMenuButtonVisible(nv);
            tableOrig.setTableMenuButtonVisible(nv);
        });
        
        CheckBox showData = new CheckBox("Show Data");
        showData.setSelected(true);
        showData.selectedProperty().addListener((obs, ov, nv) -> {
            table.setItems(nv ? data : null);
            tableOrig.setItems(nv ? dataOrig : null);
        });
        
        controls.getChildren().addAll(new Label("Common API"), new Separator(), 
                tableEditionEnabled, columnsEditionEnabled, 
                cellSelectionEnabled, selectionMultiple, columnPolicy, fixedCellSize, showTableMenuButton, showData);
        
        CheckBox showRowHeader = new CheckBox("Show Row Header");
        showRowHeader.selectedProperty().addListener((obs, ov, nv) -> {
            table.setRowHeaderVisible(nv);
        });
        
        CheckBox columnFixing = new CheckBox("Column Fixing Enabled");
        columnFixing.setSelected(true);
        columnFixing.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnFixingEnabled(nv);
        });
        
        controls.getChildren().addAll(new Label("TableView2 API"), new Separator(), showRowHeader, columnFixing);
        controls.setAlignment(Pos.CENTER_LEFT);
        controls.setMinWidth(200);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
