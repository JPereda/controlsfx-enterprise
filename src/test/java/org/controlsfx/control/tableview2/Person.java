package org.controlsfx.control.tableview2;

import java.time.LocalDate;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Person {

    private final StringProperty firstName = new SimpleStringProperty();
    private final StringProperty lastName = new SimpleStringProperty();
    private final StringProperty streetAddress = new SimpleStringProperty();
    private final StringProperty city = new SimpleStringProperty();
    private final StringProperty zip = new SimpleStringProperty();
    private final StringProperty email = new SimpleStringProperty();
    private final BooleanProperty active = new SimpleBooleanProperty();
    private final ObjectProperty<LocalDate> birthday = new SimpleObjectProperty<>();

    public final LocalDate getBirthday() {
        return birthday.get();
    }

    public final void setBirthday(LocalDate value) {
        birthday.set(value);
    }

    public final ObjectProperty<LocalDate> birthdayProperty() {
        return birthday;
    }
    

    public final StringProperty firstNameProperty() {
        return this.firstName;
    }

    public final java.lang.String getFirstName() {
        return this.firstNameProperty().get();
    }

    public final void setFirstName(final java.lang.String firstName) {
        this.firstNameProperty().set(firstName);
    }

    public final StringProperty lastNameProperty() {
        return this.lastName;
    }

    public final java.lang.String getLastName() {
        return this.lastNameProperty().get();
    }

    public final void setLastName(final java.lang.String lastName) {
        this.lastNameProperty().set(lastName);
    }

    public final StringProperty streetAddressProperty() {
        return this.streetAddress;
    }

    public final java.lang.String getStreetAddress() {
        return this.streetAddressProperty().get();
    }

    public final void setStreetAddress(final java.lang.String streetAddress) {
        this.streetAddressProperty().set(streetAddress);
    }

    public final StringProperty cityProperty() {
        return this.city;
    }

    public final java.lang.String getCity() {
        return this.cityProperty().get();
    }

    public final void setCity(final java.lang.String city) {
        this.cityProperty().set(city);
    }

    public final StringProperty zipProperty() {
        return this.zip;
    }

    public final java.lang.String getZip() {
        return this.zipProperty().get();
    }

    public final void setZip(final java.lang.String zip) {
        this.zipProperty().set(zip);
    }

    public final StringProperty emailProperty() {
        return this.email;
    }

    public final java.lang.String getEmail() {
        return this.emailProperty().get();
    }

    public final void setEmail(final java.lang.String email) {
        this.emailProperty().set(email);
    }

    public final BooleanProperty activeProperty() {
        return this.active;
    }

    public final boolean isActive() {
        return this.activeProperty().get();
    }

    public final void setActive(final boolean active) {
        this.activeProperty().set(active);
    }

    public Person (String firstName, String lastName, String address, 
            String city, String zip, boolean active, LocalDate birthday){
        this.firstName.set(firstName);
        this.lastName.set(lastName);
        this.streetAddress.set(address);
        this.city.set(city);
        this.zip.set(zip);
        this.active.set(active);
        this.birthday.set(birthday);
    }

    @Override
    public String toString() {
        return /*"Person{" + */"firstName=" + firstName.get() /*+ ", lastName=" + lastName.get() + 
                ", streetAddress=" + streetAddress.get() + ", city=" + city.get() + 
                ", zip=" + zip.get() + ", email=" + email.get() + ", active=" + active.get() + 
                ", birthday=" + birthday.get()+ '}'*/;
    }

    public Person() {
        this.firstName.set("");
        this.lastName.set("");
        this.streetAddress.set("");
        this.city.set("");
        this.zip.set("");
        this.active.set(false);
        this.birthday.set(LocalDate.now());
    }
    
    public static ObservableList<Person> generateData(int numberOfPeople){
        ObservableList<Person> persons = FXCollections.observableArrayList();
        
        for (int i = 0; i < numberOfPeople; i++) {
            persons.add(new Person("First Name:  " + i%20, "Last Name: " + i%10, "I Live Here: " + i%5, "City: " + i%3, "Zip: " + i, i%10 != 0, LocalDate.of(1900+i, 1+i%11, 1+i%29)));
        }
        
        return persons;
    }
}
