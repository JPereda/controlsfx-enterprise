/**
 * Copyright (c) 2013, 2017 ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.controlsfx.control.tableview2;

import static impl.org.controlsfx.i18n.Localization.asKey;
import static impl.org.controlsfx.i18n.Localization.localize;
import java.util.Optional;
import java.util.function.Consumer;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * A {@link TableView2} is made up of a number of {@link TableColumn2}
 * instances.
 * 
 * <h3>Configuration</h3> TableColumns2 are instantiated by the
 * {@link TableView2} itself, so there is no public constructor for this
 * class. To access the available columns, you need to call
 * {@link TableView2#getColumns()}.
 * 
 * <p>
 TableColumn2 gives you the ability to modify some aspects of the column,
 for example the {@link #setPrefWidth(double) width} or
 * {@link #setResizable(boolean) resizability} of the column.
 * 
 * <p>
 * You have the ability to fix this column at the left of the TableView2 by
 * calling {@link #setFixed(boolean)}. But you are strongly advised to check if
 * it is possible with {@link #isColumnFixable()} before calling
 * {@link #setFixed(boolean)}. Take a look at the {@link TableView2}
 * description to understand the fixing constraints.
 * 
 * <p>
 * If the column can be fixed, a {@link ContextMenu} will appear if the user right-clicks on it. 
 * If not, nothing will appear and the user will not have the possibility to fix it.
 * 
 * <h3>Screenshot</h3>
 * The column <b>A</b> is fixed and is covering column <b>B</b> and partially
 * column <b>C</b>. The context menu is being shown and offers the possibility
 * to unfix the column.
 * 
 * <br>
 * <br>
 * <center><img src="fixedColumn.png" alt="Screenshot of TableColumn2"></center>
 * 
 * @param <S> The type of the objects contained within the TableView2 items list.
 * @param <T> The type of the content in all cells in this TableColumn2.
 * @see TableView2
 */
public class TableColumn2<S, T> extends TableColumn<S, T> {

    /***************************************************************************
     * * Constructor * *
     **************************************************************************/
    
    public TableColumn2() {
        super();
        tableViewProperty().addListener(o -> init());
    }
    
    public TableColumn2(String columnHeader) {
        this();
        setText(columnHeader);
    }
    
    private void init() {
        
        setMinWidth(0);

        // The contextMenu creation must be on the JFX thread
        setContextMenu(getColumnContextMenu());

        // When changing frozen fixed columns, we need to update the ContextMenu.
        runOnTableView2(tableView2 -> 
                tableView2.columnFixingEnabledProperty().addListener((obs, oldValue, newValue) -> 
                    setContextMenu(getColumnContextMenu())));
        // When control changes items, we need to update the ContextMenu.
        runOnTableView2(tableView2 -> 
                tableView2.itemsProperty().addListener((obs, oldValue, newValue) -> 
                    setContextMenu(getColumnContextMenu())));
    }

    /***************************************************************************
     * * Public Methods * *
     **************************************************************************/
    
    /**
     * Returns whether this column is fixed or not.
     * 
     * @return true if this column is fixed.
     */
    public final boolean isFixed() {
        return getTableView2()
                .map(tableView2 -> tableView2.getFixedColumns().contains(this))
                .orElse(false);
    }

    /**
     * Fixes this column to the left if possible, although it is recommended that
     * you call {@link #isColumnFixable()} before trying to fix a column.
     *
     * If you want to fix several columns (because of a span for example), add
     * all the columns directly in {@link TableView2#getFixedColumns() }.
     * Always use {@link TableView2#areTableViewColumnsFixable(java.util.List)} before.
     *
     * @param fixed if true, fixes the columns in the fixed columns list
     */
    public final void setFixed(boolean fixed) {
        runOnTableView2(tableView2 -> {
            if (fixed) {
                tableView2.getFixedColumns().add(this);
            } else {
                tableView2.getFixedColumns().removeAll(this);
            }
        });
    }

    /**
     * Indicate whether this column can be fixed or not. Call that method before
     * calling {@link #setFixed(boolean)} or adding an item to
     * {@link TableView2#getFixedColumns()}.
     *
     * A column cannot be fixed alone if any cell inside the column has a column
     * span superior to one.
     *
     * @return true if this column is fixable.
     */
    public final boolean isColumnFixable() {
        return getParentColumn() == null;
    }

    /***************************************************************************
     * * Private Methods * *
     **************************************************************************/

    /**
     * Generate a context Menu in order to fix/unfix some column It is shown
     * when right-clicking on the column header
     * 
     * @return a context menu.
     */
    private ContextMenu getColumnContextMenu() {
        final ContextMenu contextMenu = new ContextMenu();
        if (isColumnFixable() && getTableView2()
                .map(TableView2::isColumnFixingEnabled).orElse(false)) {
            
            final MenuItem fixItem = new MenuItem(localize(asKey("tableview2.column.menu.fix"))); //$NON-NLS-1$
            contextMenu.setOnShowing(e -> {
                if (!isFixed()) {
                    fixItem.setText(localize(asKey("tableview2.column.menu.fix"))); //$NON-NLS-1$
                } else {
                    fixItem.setText(localize(asKey("tableview2.column.menu.unfix"))); //$NON-NLS-1$
                }
            });
            fixItem.setGraphic(new ImageView(new Image(TableColumn2.class.getResourceAsStream("pinTableView2.png")))); //$NON-NLS-1$
            fixItem.setOnAction(e -> setFixed(!isFixed()));
            contextMenu.getItems().addAll(fixItem);
        }
            
        return contextMenu;
    }

    private void runOnTableView2(Consumer<TableView2> consumer) {
        getTableView2().ifPresent(consumer);
    }
     
    private Optional<TableView2<S>> getTableView2() {
        TableView<S> tableView = getTableView();
        if (tableView != null && tableView instanceof TableView2) {
            return Optional.of((TableView2<S>) tableView);
        }
        return Optional.empty();
    }
}
