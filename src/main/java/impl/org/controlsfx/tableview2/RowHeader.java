/**
 * Copyright (c) 2013, 2017 ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package impl.org.controlsfx.tableview2;

import static impl.org.controlsfx.i18n.Localization.asKey;
import static impl.org.controlsfx.i18n.Localization.localize;

import java.util.BitSet;
import java.util.Set;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Callback;
import org.controlsfx.control.tableview2.TableView2;

/**
 * Display the row header on the left of the cells (view), where the user can
 * display any content via {@link TableView2#getRowHeaders() }.
 */
public class RowHeader extends StackPane {

    private static final String TABLE_ROW_KEY = "TableRow"; //$NON-NLS-1$
    private static final Image pinImage = new Image(TableView2.class.getResource("pinTableView2.png").toExternalForm()); //$NON-NLS-1$

    /**
     * *************************************************************************
     * * Private Fields * *
     * ************************************************************************
     */
    private final TableView2<?> tableView;
    private TableView2Skin<?> skin;
    private double horizontalHeaderHeight;
    /**
     * This represents the RowHeader width. It's the total amount of space
 used by the RowHeader {@link TableView2#getRowHeaderWidth() }.
     *
     */
    private final DoubleProperty innerRowHeaderWidth = new SimpleDoubleProperty();
    private Rectangle clip; // Ensure that children do not go out of bounds
    private ContextMenu blankContextMenu;

    // used for column resizing

    /**
     * This BitSet keeps track of the selected rows (when clicked on their
     * header) in order to allow multi-resize.
     */
    private final BitSet selectedRows = new BitSet();

    /**
     * ****************************************************************
     * CONSTRUCTOR
     *
     * @param tableView
     * ***************************************************************
     */
    public RowHeader(TableView2<?> tableView) {
        this.tableView = tableView;
    }

    /**
     * *************************************************************************
     * * Private/Protected Methods *
     * ***********************************************************************
     */
    /**
     * Init
     *
     * @param skin
     * @param horizontalHeader
     */
    void init(final TableView2Skin<?> skin, TableHeaderRow2 horizontalHeader) {
        this.skin = skin;
        // Adjust position upon TableHeaderRow2 height
        horizontalHeader.heightProperty().addListener((obs, oldHeight, newHeight) -> {
            horizontalHeaderHeight = newHeight.doubleValue();
            requestLayout();
        });

        // Clip property to stay within bounds
        clip = new Rectangle(getRowHeaderWidth(), 
                snapSize(tableView.getHeight() - tableView.snappedTopInset() - tableView.snappedBottomInset()));
        clip.relocate(snappedTopInset(), snappedLeftInset());
        clip.setSmooth(false);
        clip.heightProperty().bind(Bindings.createDoubleBinding(() -> 
                tableView.getHeight() - tableView.snappedTopInset() - tableView.snappedBottomInset(), 
                tableView.heightProperty()));
        clip.widthProperty().bind(innerRowHeaderWidth);
        RowHeader.this.setClip(clip);

        // We desactivate and activate the rowHeader upon request
        tableView.rowHeaderVisibleProperty().addListener(layout);

        // When the Column header is showing or not, we need to update the
        // position of the rowHeader
        tableView.getFixedRows().addListener(layout);
        tableView.rowFixingEnabledProperty().addListener(layout);
        tableView.rowHeaderWidthProperty().addListener(layout);

        // In case we resize the view in any manners
        tableView.heightProperty().addListener(layout);

        // For layout properly the rowHeader when there are some selected
        // items
        skin.getSelectedRows().addListener(layout);
        
        skin.getHBar().visibleProperty().addListener(layout);

        blankContextMenu = new ContextMenu();
    }

    public double getRowHeaderWidth() {
        return innerRowHeaderWidth.get();
    }

    public ReadOnlyDoubleProperty rowHeaderWidthProperty() {
        return innerRowHeaderWidth;
    }

    public double computeHeaderWidth() {
        double width = 0;
        if (tableView.isRowHeaderVisible()) {
            width += tableView.getRowHeaderWidth();
        }
        return width;
    }

    void clearSelectedRows() {
        selectedRows.clear();
    }

    @Override
    protected void layoutChildren() {
        if (tableView.isRowHeaderVisible() && skin.getCellsSize() > 0) {

            double x = snappedLeftInset();
            innerRowHeaderWidth.setValue(0);
            if (tableView.isRowHeaderVisible()) {
                innerRowHeaderWidth.setValue(getRowHeaderWidth() + tableView.getRowHeaderWidth());
            }

            getChildren().clear();

            final int cellSize = skin.getCellsSize();

            Label label;

            addVisibleRows(x, cellSize);

            addFixedRows(x, cellSize);

            // First one blank and on top (z-order) of the others
            if (skin.isColumnHeaderVisible()) {
                label = getLabel(null);
                label.setOnMousePressed((MouseEvent event) -> {
                    tableView.getSelectionModel().clearSelection();
                    tableView.getSelectionModel().selectAll();
                });
                label.setText(""); //$NON-NLS-1$
                label.resize(tableView.getRowHeaderWidth(), horizontalHeaderHeight);
                label.layoutYProperty().unbind();
                label.setLayoutY(0);
                label.setLayoutX(x);
                label.getStyleClass().clear();
                label.setContextMenu(blankContextMenu);
                getChildren().add(label);
            }

            ScrollBar hbar = skin.getHBar();
            //FIXME handle height.
            if (hbar.isVisible()) {
                // Last one blank and on top (z-order) of the others
                label = getLabel(null);
                label.getProperties().put(TABLE_ROW_KEY, null);
                label.setText(""); //$NON-NLS-1$
                label.resize(getRowHeaderWidth(), snapSize(hbar.getHeight()));
                label.layoutYProperty().unbind();
                label.relocate(snappedLeftInset(), getHeight() - snappedBottomInset() - snapSize(hbar.getHeight()));
                label.getStyleClass().setAll("hbar");
                label.setContextMenu(blankContextMenu);
                getChildren().add(label);
            }
        } else {
            getChildren().clear();
            innerRowHeaderWidth.setValue(0);
        }
    }

    /**
     * Returns true if there are no fixed rows
     *
     * @param tableView the {@link TableView2}
     * @return Returns true if there are no fixed rows 
     */
    public static boolean isFixedRowEmpty(TableView2<?> tableView) {
        return tableView.getFixedRows().isEmpty();
    }

    private void addFixedRows(double x, int cellSize) {
        int viewRow;
        Label label;
        final Set<Integer> currentlyFixedRow = skin.getCurrentlyFixedRow();
        // Then we iterate over the FixedRows if any
        if (!isFixedRowEmpty(tableView) && cellSize != 0) {
            for (int j = 0; j < tableView.getFixedRows().size(); ++j) {

                viewRow = tableView.getFixedRows().get(j);
                if (!currentlyFixedRow.contains(viewRow)) {
                    break;
                }

                if (tableView.isRowHeaderVisible()) {
                    label = getLabel(viewRow);
                    TableRow<?> row = skin.getRowIndexed(viewRow);
                    label.getProperties().put(TABLE_ROW_KEY, row);
                    label.setText(getRowHeader(viewRow));
                    label.resize(tableView.getRowHeaderWidth(), skin.getRowHeight(viewRow));
                    label.setContextMenu(getRowContextMenu(viewRow));
                    if (row != null && row instanceof TableRow2) {
                        label.layoutYProperty().bind(row.layoutYProperty().add(horizontalHeaderHeight).add(((TableRow2<?>) row).verticalShift));
                    }
                    label.setLayoutX(x);
                    final ObservableList<String> css = label.getStyleClass();
                    if (skin.getSelectedRows().contains(viewRow)) {
                        css.addAll("selected"); //$NON-NLS-1$
                    } else {
                        css.removeAll("selected"); //$NON-NLS-1$
                    }
                    if (tableView.getItems() != null) {
                        css.addAll("fixed"); //$NON-NLS-1$
                    }
                    getChildren().add(label);
                }
            }
        }
    }

    private void addVisibleRows(double x, int cellSize) {
        int rowIndex;

        Label label;

        int i = 0;

        TableRow<?> row = skin.getRow(i);

        double rowHeaderWidth = tableView.getRowHeaderWidth();
        double height;
        // We iterate over the visibleRows
        while (cellSize != 0 && row != null) {
            rowIndex = row.getIndex();
            height = row.getHeight();

            if (tableView.isRowHeaderVisible()) {
                label = getLabel(rowIndex);
                label.getProperties().put(TABLE_ROW_KEY, row);
                label.setText(getRowHeader(rowIndex));
                label.resize(rowHeaderWidth, height);
                label.setLayoutX(x);
                label.layoutYProperty().bind(row.layoutYProperty().add(horizontalHeaderHeight));
                label.setContextMenu(getRowContextMenu(rowIndex));

                getChildren().add(label);
                // We want to highlight selected rows
                final ObservableList<String> css = label.getStyleClass();
                if (skin.getSelectedRows().contains(rowIndex)) {
                    css.addAll("selected"); //$NON-NLS-1$
                } else {
                    css.removeAll("selected"); //$NON-NLS-1$
                }
                if (tableView.getFixedRows().contains(rowIndex)) {
                    css.addAll("fixed"); //$NON-NLS-1$
                } else {
                    css.removeAll("fixed"); //$NON-NLS-1$
                }

            }
            row = skin.getRow(++i);
        }
    }


    /**
     * Create a new label and put it in the pile or just grab one from the pile.
     *
     * @return
     */
    private Label getLabel(Integer row) {
        Label label = new Label();
        // We want to select the whole row when clicking on a header.
        label.setOnMousePressed(row == null ? null : (MouseEvent event) -> {
            if (event.isPrimaryButtonDown()) {
                if (event.getClickCount() == 2) {
                    requestLayout();
                } else {
                    headerClicked(row, event);
                }
            }
        });
        return label;
    }

    /**
     * If a header is clicked, we must select the whole row. If Control key of
     * Shift key is pressed, we must not deselect the previous selection but
     * just act like the {@link TableViewBehavior} would.
     *
     * @param row
     * @param event
     */
    private void headerClicked(int row, MouseEvent event) {
        TableViewSelectionModel sm = tableView.getSelectionModel();
        int focusedRow = sm.getFocusedIndex();
        int rowCount = skin.getItemCount();
        ObservableList<TableColumn<?, ?>> columns = sm.getTableView().getColumns();
        TableColumn<?, ?> firstColumn = columns.get(0);

        if (event.isShortcutDown()) {
            if (sm.getSelectionMode() == SelectionMode.MULTIPLE) {
                sm.select(row);
                BitSet tempSet = (BitSet) selectedRows.clone();
                selectedRows.or(tempSet);
            } else {
                sm.clearSelection();
                sm.select(row);
            }
            selectedRows.set(row);
        } else if (event.isShiftDown() && focusedRow >= 0 && focusedRow < rowCount) {
            sm.clearSelection();
            if (sm.getSelectionMode() == SelectionMode.MULTIPLE) {
                sm.selectRange(focusedRow, row);
            } else {
                focusedRow = row;
            }
            sm.select(row);
            //We want to let the focus on the focused row.
            sm.getTableView().getFocusModel().focus(focusedRow, firstColumn);
            int min = Math.min(row, focusedRow);
            int max = Math.max(row, focusedRow);
            selectedRows.set(min, max + 1);
        } else {
            sm.clearSelection();
            sm.select(row);
            //And we want to have the focus on the first cell in order to be able to copy/paste between rows.
            selectedRows.set(row);
            sm.getTableView().getFocusModel().focus(row, firstColumn);
        }
    }

    /**
     * Return a contextMenu for fixing a row if possible.
     *
     * @param row
     * @return
     */
    private ContextMenu getRowContextMenu(final Integer row) {
        if (tableView.getItems() != null && tableView.isRowFixable(row)) {
            final ContextMenu contextMenu = new ContextMenu();

            MenuItem fixItem = new MenuItem(localize(asKey("tableview2.rowheader.menu.fix"))); //$NON-NLS-1$
            contextMenu.setOnShowing(e -> {
                if (tableView.getFixedRows().contains(row)) {
                    fixItem.setText(localize(asKey("tableview2.rowheader.menu.unfix"))); //$NON-NLS-1$
                } else {
                    fixItem.setText(localize(asKey("tableview2.rowheader.menu.fix"))); //$NON-NLS-1$
                }
            });
            fixItem.setGraphic(new ImageView(pinImage));

            fixItem.setOnAction(e -> {
                if (tableView.getFixedRows().contains(row)) {
                    tableView.getFixedRows().remove(row);
                } else {
                    tableView.getFixedRows().add(row);
                }
            });
            contextMenu.getItems().add(fixItem);

            return contextMenu;
        } else {
            return blankContextMenu;
        }
    }

    /**
     * Return the String header associated with this row index.
     *
     * @param index
     * @return
     */
    private String getRowHeader(int index) {
        final Callback<Integer, String> rowHeaderFactory = tableView.getRowHeaderFactory();
        if (rowHeaderFactory != null) {
            return rowHeaderFactory.call(index);
        }
        return "";
    }

    /**
     * *************************************************************************
     * * Listeners * *
     * ************************************************************************
     */
    private final InvalidationListener layout = (Observable arg0) -> {
        requestLayout();
    };
}
